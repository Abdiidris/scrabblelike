package com.coke.game.controller;

import java.util.ArrayList;

import com.coke.game.model.*;

//B3, DOWN, 513
public class GameController implements Controller {

    Board board;
    Rack rack;
    int points;
    Play lastPlay;
    ArrayList<Integer> usedTiles;

    public GameController() {
        board = new Board(10, 10);
        rack = new Rack(5);
        points = 0;
        usedTiles = new ArrayList<>();

    }

    @Override
    public String refillRack() {
        rack.refill();

        return rack.toString();

    }

    @Override
    public String gameState() {

        return "\n Score: " + String.valueOf(points)+"\n"+board.toString() + "\n" + rack.toString();
    }

    @Override
    public String play(Play play) {
        PlayResult result = runPlay(play);
        if (result != null) {

            String resultStr = result.toString();

            points += result.getPoints();

            for (Integer i : usedTiles) {
                rack.remove(i);
            }
            board.registerMove();

            return "\n -------> " + resultStr + "\n";

        }

        return "\n -------> " + "No such word" + "\n";
    }

    public Tile[] getSelectedTiles(Play play) {
        Tile[] selectedTiles = new Tile[play.letterPositionsInRack().length()];
        char[] tileNums = play.letterPositionsInRack().toCharArray();
        for (int i = 0; i < tileNums.length; i++) {
            selectedTiles[i] = (rack.getTile(Character.getNumericValue(tileNums[i]) - 1));

            usedTiles.add(Character.getNumericValue(tileNums[i]) - 1);

        }

        return selectedTiles;
    }

    @Override
    public String calculateScore(Play play) {

        PlayResult result = runPlay(play);
        if (result != null) {

            String resultStr = String.valueOf(result.getPoints());

            board.cancelMove();
            usedTiles = new ArrayList<>();
            return "\n -------> " + resultStr + "\n";

        }

        return "\n -------> NO POINTS \n";
    }

    public PlayResult runPlay(Play play) {
        Tile[] selectedTiles = getSelectedTiles(play);

        // get initial cell
        int Y = play.cell().toCharArray()[0] - 65; // A == 65 B==66..
        int X = Character.getNumericValue(play.cell().toCharArray()[1]);

        return board.addTiles(selectedTiles, X, Y, (play.dir() != Direction.ACROSS));

    }

    @Override
    public String checkValidity(Play play) {

        PlayResult result = runPlay(play);
        if (result != null) {
            board.cancelMove();
            usedTiles = new ArrayList<>();
            return "\n -------> VALID \n";

        }

        return "\n -------> INVLAID \n";
    }

}