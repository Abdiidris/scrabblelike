package com.coke.game.model;

import java.util.ArrayList;
import java.util.Set;
import java.util.Stack;

public class PlayResult { // records the cells that have been added tiles, the points earned and the words
                          // made

  private Stack<Cell> done;
  private Stack<Cell> undone; // records placements and stores the placement and the tile it was placed
  private int points;
  private ArrayList<String> words;
  private String resultingWord;
  
  public PlayResult() {
    done = new Stack<Cell>();
    undone = new Stack<Cell>();
    words = new ArrayList<>();
    resultingWord = "";

  }

  public void addWord(String word) {
    words.add(word);
  }
  public String getResultingWord()
  {
    return resultingWord;
  }
  public int getPoints() {
    return points;
  }

  public void doPlacement(Cell cell) {
    done.push(cell);
    resultingWord = resultingWord + cell.content.value;
    points += cell.sumPoints();
  }

  public Cell undoPlacement() {

    points -= done.peek().sumPoints();
    undone.push(done.peek());
    
    // remove the char at the end of the word
    resultingWord = resultingWord.substring(0,resultingWord.length()-1);

    return done.pop();

  }

  public void clean() {
    done = new Stack<Cell>();
    undone = new Stack<Cell>();
    points = 0;
    words = new ArrayList<>();
  }

  public boolean allUndone() {
    return done.isEmpty();
  }

  public void redo() {
    points += undone.peek().sumPoints();
    done.push(undone.pop());
  }

  @Override
  public String toString() {
    String toStr = " Words made : ";
    
    for (String word : words) {
      toStr += word + ", ";
    }

    toStr = "\n points: " + this.points;

    return toStr;
  }
}