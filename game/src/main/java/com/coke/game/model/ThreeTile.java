package com.coke.game.model;

public class ThreeTile extends Tile {

    public ThreeTile() {
        super();
        this.value = this.getRandomChar(new char[] { 'q', 'x', 'y', 'z' });
    }

    public ThreeTile(char value)
    {
        super(value);
    }
    @Override
    public int getPoints() {
        return 3;
    }

}