package com.coke.game.model;

public class StandardCell extends Cell {

    public StandardCell(int x, int y) {
        super(x, y);
     
    }
    
    @Override
    public int sumPoints() {
        return content.getPoints();
    }

}