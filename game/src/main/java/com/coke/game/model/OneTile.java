package com.coke.game.model;
public class OneTile extends Tile {
   
    public OneTile() {
        super();
        this.value = getRandomChar(new char[] { 'a', 'c', 'd', 'e', 'f', 'h', 'i', 'l', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w'});
    }
    public OneTile(char value) {
        super(value);
    }
    @Override
    public int getPoints() {
        return 1;
    }


}