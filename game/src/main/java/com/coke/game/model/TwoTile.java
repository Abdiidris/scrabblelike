package com.coke.game.model;

public class TwoTile extends Tile {
   
    public TwoTile() {
        super();
        this.value = getRandomChar(new char[] { 'b', 'g', 'j', 'k', 'm', 'n'});
    }
    public TwoTile(char value)
    {
        super(value);   
    }
    @Override
    public int getPoints() {
        return 2;
    }


}