package com.coke.game.model;

public class CellKey extends Object {


    int x;
    int y;
    public CellKey(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o.getClass() == this.getClass()) {
            
            CellKey compCellKey = (CellKey)o;

            return compCellKey.getX() == this.getX() &&
            compCellKey.getY()==this.getY();
        }

        return false;
    }
    @Override
    public int hashCode(){
        // convert them to string and then out togther
        return (getX()*10) + getY();
    }
}