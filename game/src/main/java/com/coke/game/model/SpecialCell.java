package com.coke.game.model;

public class SpecialCell extends Cell {
    
   
    public SpecialCell(int x, int y) {
        super(x, y);
       
    }

    @Override
    public int sumPoints() {
    if (this.content == null) {
        return 0;
    }
        return content.getPoints() * 2;
    }

}