package com.coke.game.model;

import java.util.HashSet;
import java.util.Random;


public class Board {

  
    Cells<Cell> cells;
    int numRows;
    int numColumns;

    public Board(int numRows, int numColumns) {
       
        cells = new Cells<Cell>(numRows*numColumns);
      
        this.numColumns = numColumns;
        this.numRows = numRows;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++) {
                cells.add(new StandardCell(i, j));
            }
        }

        addSpecialCells(8);

    }

    public void registerMove()
    {
        cells.commit();
    }

    public void cancelMove()
    {
        cells.cancel();
    }
    public String toString() {

        String result = "";
        result += "      A  B  C  D  E  F  G  H  I  J  ";
        for (int i = 0; i < numRows; i++) {
           
            result += "\n";
            result += "    -------------------------------";
            
            result += "\n "+ (i+1) + "  |";
       

            for (int j = 0; j < numColumns; j++) {
                int key = (i * 10) + j;
                // result +=" " + i + " " + j;
               
                if (cells.get(key).isEmpty()) {
                    if (cells.get(key) instanceof SpecialCell) {
                        result += " +";
                        result += "|";
                    } else {
                        result += "  |";
                    }

                } else {

                    result += " " + cells.get(key).content.getValue() + "|";

                }

            }

        }
        result += "\n    -------------------------------";
        return result;
    }

    public PlayResult addTiles(Tile[] tiles, int startX, int startY, Boolean across) {
        startX-=1; 
        // because list startss from 0
        int hasCode = ((startX * 10) + startY);
        return cells.placeTiles(tiles, hasCode, across);
    }

    
    private void addSpecialCells(int size) // selects 8 random locations
    {
        HashSet<Integer> cellLocation = new HashSet<>();

        Random rnd = new Random();
        // returns 8 random locations
        int created = 0;
        while (created < size) {
            int x = rnd.nextInt(this.numRows);
            int y = rnd.nextInt(this.numColumns);
            int key = x * 10 + y;
            if (!cellLocation.contains(key)) {
                created++;
                cellLocation.add(key);
                cells.add(key,new SpecialCell(x, y));
            }
        }

    }

}