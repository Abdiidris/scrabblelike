package com.coke.game.model;

public abstract class Cell {

    Tile content;
    int x;
    int y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        content=null;
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o.getClass() == this.getClass()) {
            
            Cell compCellKey = (Cell)o;

            return compCellKey.hashCode()==this.hashCode();
        }

        return false;
    }

    @Override
    public int hashCode(){
        // convert them to string and then out togther
        return (getX()*10) + getY();
    }

    public boolean isEmpty() {
        return content == null;
    }

    public Tile getContent() {
        return content;
    }

    public void setContent(Tile tile) {
        this.content = tile;
    }

    public abstract int sumPoints();// works out amount of points for the letter in this cell

}