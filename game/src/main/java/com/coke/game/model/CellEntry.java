package com.coke.game.model;

import java.util.Map.Entry;
public class CellEntry<K,V> implements Entry<K,V>{

    V value;
    K key;
    public CellEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(Object value) {
        this.value = (V) value;
        return this.value;
    }

}