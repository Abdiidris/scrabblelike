package com.coke.game.model;

import java.util.ArrayList;
import java.util.Random;

public class Rack {
    ArrayList<Tile> tiles; // because the size of the array changes frequently
    int size;
    public Rack(int size) {
        tiles = new ArrayList<>();
        this.size=size;

        refill();

    }
    public void refill() // T = n - numberOfRemovals
    {
     
      
       for (int i = 0; i < size-tiles.size(); i++) {
           tiles.add(getRandomTile());
        }
    }
    @Override
    public String toString() // n
    {   
        String result="";
        for (int i = 0; i < getSize(); i++) {
            result +=  i+1 + ")  " + tiles.get(i).value + " Points " + tiles.get(i).getPoints() + " \n";
            //          ! so that the user's list doesnt start from 0
        }

        return result;
    }
    public int getSize(){ 
        return tiles.size();
    }
    public void remove(int index){
         tiles.remove(index);
    }
    public Tile getTile(int index)
    {
        
        return tiles.get(index);
    }
    private Tile getRandomTile() {
        int choice = new Random().nextInt(3);

        switch (choice) {
        case 0:
            return new ThreeTile();
        case 1:
            return new TwoTile();
        case 2:
            return new OneTile();

        default:
            return null;

        }

    }

}
