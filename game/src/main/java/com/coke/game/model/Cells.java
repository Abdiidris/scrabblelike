package com.coke.game.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import com.coke.game.data.*;

public class Cells<T extends Cell> implements List<T> {

    T[] cells;

    PlayResult playResult;
    Words words;
    int numOfTilesPlaced;
    boolean firstTurn;
    public Cells(int capacity) { // playResultime number
        cells = (T[]) new Cell[capacity];
        playResult = new PlayResult();
        words = new Words();
        numOfTilesPlaced = 0;
        firstTurn=true;
    }

    @Override
    public void clear() {
        cells = (T[]) new Cell[cells.length];
        numOfTilesPlaced = 0;
    }

    // move to Board
    public PlayResult placeTiles(Tile[] tiles, int StartingCellKey, Boolean across) {
        // returns number of points earned from placing tiles

        Cell cell = this.get(StartingCellKey);
        for (Tile tile : tiles) {

            // check validity of placing a tile
            // place the tile and then check the validity
            cell.setContent(tile);
            this.add(cell.hashCode(), (T) cell); // replaces old cell
            playResult.doPlacement(cell);
            if (validTilePlacment(cell, across)) {

                numOfTilesPlaced++;

                if (across) {
                    cell = this.getNext(cell, 1, 0);
                } else {
                    cell = this.getNext(cell, 0, 1);
                }
            } else {
                // since it's failed clean the work tracking
                cancel();
                return null;
            }

        }
        
        // now we can check whether the resulting makes sense on it's direction
        String fullWord;
        if (!across) { // we check the word it makes vertically
            // get the words to the left and the right
            fullWord = getWordVertical(cell);
        } else {
            fullWord = getWordHorizontal(cell);
        }
        if (fullWord != "" && words.isWord(fullWord)) {
            return playResult; // if the newly created item is also a word
        }

        cancel();
        return null;

    }

    public PlayResult commit() {
        // since it's done clean the work tracking
        PlayResult playResultRet = playResult; // the pr to be returned
        playResult.clean();
        firstTurn=false;
        return playResultRet;

    }
    public void cancel() {
        while (!playResult.allUndone()) {
            Cell cell = playResult.undoPlacement();
            reset(cell);
        }

        // if this cancellation clears the board then the next go will be a first go
        firstTurn=numOfTilesPlaced==0;
            
        
        playResult.clean();
    }

    public boolean isAlone(Cell cell) {
        return ( // if it's the only char hor and vert
        getWordHorizontal(cell).equals(String.valueOf(cell.content.value))
                && getWordVertical(cell).equals(String.valueOf(cell.content.value)));
    }

    public boolean validTilePlacment(Cell cell, Boolean across) {
        // check if it's next to another word
        // check if the word it makes (opposite to the current play direction) is valid

        if (isEmpty()) {
            return true;
        }
        // ensure it's getting placed next to another tile
        if (isAlone(cell)) {
            return false;
        }

        String word;

        if (across) { // we check the word it makes vertically
            // get the words to the left and the right
            word = getWordVertical(cell);
        } else {
            word = getWordHorizontal(cell);
        }

        // if it's the first word on the board, dont check whether it makes a word
        if (firstTurn) {
            return true;
        }
        if (words.isWord(word)) { // fix reverse word issue some words in th
            playResult.addWord(word);
            return true;
        } else if (word.equals(String.valueOf(cell.content.value))) {
            return true; // the new char doesnt make any unknown words as it makes nothing
        }

        return false;

    }

    public String getWordHorizontal(Cell cell) {
        String word= getWord(cell, -1,0);
        if (cell.isEmpty()) {
            word+="";
        }
        else
        {
            word+= cell.content.value;
        }
        word+=   getWord(cell, 1, 0);
        return word;
    }

    public String getWordVertical(Cell cell) {
       String word= getWord(cell, 0, -1);
        if (cell.isEmpty()) {
            word+="";
        }
        else
        {
            word+= cell.content.value;
        }
         
        word+= getWord(cell, 0, 1);
        return word;
       
    }

    public String getWord(Cell cell, int xDir, int yDir) { // doesnt include char in starting cell
        // gets all the words from a direction to to the word
        String word = "";
        while (this.getNext(cell, xDir, yDir) != null && !this.getNext(cell, xDir, yDir).isEmpty()) {
            cell = this.getNext(cell, xDir, yDir);
            word += cell.content.value;
        }
        return word;
    }

    public Cell getNext(Cell cell, int xDir, int yDir) {
        // x 1 y 1 would return one done and right
        int x = (cell.getX() + xDir);
        int y = +(cell.getY() + yDir);

        if (x < 0 || y < 0) {
            return null; // outside of boundary
        }

        return this.get((x * 10) + y);
    }

    public boolean withinBounds(int index) {
        return (index < cells.length && index >= 0);
    }

    @Override
    public boolean isEmpty() {
        return numOfTilesPlaced == 0;
    }

    /*
     * @Override public V put(K key, V value) {
     * 
     * if (withinBounds(key.hashCode())) {
     * 
     * size++; cells[key.hashCode()] = new CellEntry<K, V>(key, value); return
     * entries[key.hashCode()].getValue(); }
     * 
     * return null; }
     */

    public void reset(Cell cell) {
        // replace the cell
        numOfTilesPlaced--;
        cell.setContent(null);
        this.add(cell.hashCode(), (T) cell);
        // this.put((Integer) (cell.hashCode()), cell);
    }

    @Override
    public int size() {
        return numOfTilesPlaced;
    }

    public Cell[] getCells() {
        return this.cells;
    }

    @Override
    public boolean add(T e) {
        if (withinBounds(e.hashCode())) {
            cells[e.hashCode()] = e;
        }
        return false;
    }

    @Override
    public void add(int index, T element) {
        if (withinBounds(index)) {
            cells[index] = element;
        }

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        
        return false;
    }

    @Override
    public boolean contains(Object o) {
        // TODO Auto-generated method stub
    	
    	
    	for (int i = 0; i < cells.length; i++) {
        	if(cells[i].equals(o)) {
        		cells.hashCode();
        	}
		}

        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        // TODO Auto-generated method stub
    	
        return false;
    }

    @Override
    public T get(int index) {
        if (withinBounds(index)) {
            return cells[index];
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        // TODO Auto-generated method stub
    	
    	
    	
        return 0;
    }

    @Override
    public Iterator<T> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int lastIndexOf(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean remove(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public T remove(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public T set(int index, T element) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // TODO Auto-generated method stub
        return null;
    }
}