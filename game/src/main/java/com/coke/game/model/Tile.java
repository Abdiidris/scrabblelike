package com.coke.game.model;

import java.util.Random;

public abstract class Tile {
    protected char value;

    
    abstract public int getPoints();

    public static String getWord(Tile[] tiles)
    {
        String word ="";
        for (Tile tile : tiles) {
            word = word + String.valueOf(tile.value);
        }

        return word;
    }

    public Tile() {
        super();
    }

    public Tile(char value) {
        super();
        this.value=value;
     
    }
    public char getValue(){
        return this.value;
    }   

    protected char getRandomChar(char[] chars)
    {
       return  chars[new Random().nextInt(chars.length)];
    }
}