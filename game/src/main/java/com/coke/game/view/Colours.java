package com.coke.game.view;

public class Colours {
    public static final String r = "\u001B[0m";

    public static final String bla = "\u001B[30m";
    public static final String red = "\u001B[31m";
    public static final String gre = "\u001B[32m";
    public static final String yel = "\u001B[33m";
    public static final String blu = "\u001B[34m";
    public static final String pur = "\u001B[35m";
    public static final String cya = "\u001B[36m";
    public static final String whi = "\u001B[37m";

    public static final String brbla = "\u001B[90m";
    public static final String brred = "\u001B[91m";
    public static final String brgre = "\u001B[92m";
    public static final String bryel = "\u001B[93m";
    public static final String brblu = "\u001B[94m";
    public static final String brpur = "\u001B[95m";
    public static final String brcya = "\u001B[96m";
    public static final String brwhi = "\u001B[97m";
}