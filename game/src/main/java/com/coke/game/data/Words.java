package com.coke.game.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Words {////// binary sort
    List<String> words;

    public Words() {

        try {
            words = Files.readAllLines(Paths.get(
                    "./src/main/java/com/coke/game/data/wordlist.txt"),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isWord(String word) { // check noth standard and rever of the word
        StringBuilder bword = new StringBuilder(word);
        
        return Collections.binarySearch(words, word) >= 0
                || Collections.binarySearch(words, bword.reverse().toString()) >= 0;
    }
}